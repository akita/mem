module gitlab.com/akita/mem/v3

require (
	github.com/golang/mock v1.6.0
	github.com/google/btree v1.1.2
	github.com/onsi/ginkgo/v2 v2.9.5
	github.com/onsi/gomega v1.27.6
	github.com/rs/xid v1.5.0
	gitlab.com/akita/akita/v3 v3.0.0-alpha.23
)

require (
	github.com/go-logr/logr v1.2.4 // indirect
	github.com/go-sql-driver/mysql v1.7.1 // indirect
	github.com/go-task/slim-sprig v0.0.0-20230315185526-52ccab3ef572 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/google/pprof v0.0.0-20230510103437-eeec1cb781c3 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
	github.com/tebeka/atexit v0.3.0 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	golang.org/x/tools v0.9.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

// replace gitlab.com/akita/akita => ../akita

// replace gitlab.com/akita/util => ../util

go 1.20
